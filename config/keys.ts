/**
 * Including variables from .env into process.env
 * @hint
 */
require('dotenv').config();

interface IKeys {
    mongoURI: string,
    secret: string,
}

const prodKeys: IKeys = {
  mongoURI: 'placeholder',
  secret: 'placeholder',
};

const devKeys = {
  mongoURI: 'mongodb://localhost:27017/boilerplate',
  secret: 'very, very big secret',
};

export default (process.env.NODE_ENV === 'production') ? prodKeys : devKeys;

