import {Router as router, Request, Response} from 'express';
import messages from '../config/messages';

/**
 * Router intended to handle addressing to incorrect URL.
 * @hint
 */
export const index: router = router({
  strict: true,
});

index.all('*', (rq: Request, rs: Response) => {
  rs.status(418).json({message: messages.system.wrongEndpoint});
});
