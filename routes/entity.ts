import {Router as router} from 'express';
import {
  createEntity,
  updateEntity,
  getEntities,
  getEntityById,
  deleteEntity,
} from '../controllers/entity.controller';

export const entity: router = router({
  strict: true,
});

entity.post(
    '/',
    createEntity,
);

entity.get(
    '/',
    getEntities,
);

entity.get(
    '/:id',
    getEntityById,
);

entity.put(
    '/:id',
    updateEntity,
);

entity.delete(
    '/:id',
    deleteEntity,
);
