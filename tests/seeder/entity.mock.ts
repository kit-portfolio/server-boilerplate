export const entitiesMocks = [
    {
        lorem: "Outstanding entity",
        ipsum: "wow",
        dolor: "Fancy type",
        sit: 9876543210,
        amet: 11,
        consectetur: true,
        adipiscing: "1995-12-17"
    },
    {
        lorem: "Ignorable entity",
        ipsum: "That's curious",
        dolor: "Fancy type",
        sit: 9876543210,
        amet: 11,
        consectetur: true,
        adipiscing: "2020-03-14"
    },
    {
        lorem: "Proud entity",
        ipsum: "Just like a Lion",
        dolor: "Shiny type",
        sit: 7890123456,
        amet: 11,
        consectetur: true,
        adipiscing: "1900-01-01"
    },
    {
        lorem: "Villain entity",
        ipsum: "U-HA-HA-HA",
        dolor: "Shiny type",
        sit: 7890123456,
        amet: 11,
        consectetur: false,
        adipiscing: "1966-01-01"
    },
    {
        lorem: "Mysterious entity",
        dolor: "Shiny type",
        sit: 1234567890,
        amet: 6,
        consectetur: false,
        adipiscing: "2010-11-09"
    },
];
