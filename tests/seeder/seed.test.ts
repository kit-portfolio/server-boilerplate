import axios from "axios";
import {entitiesMocks} from "./entity.mock";
import {URN} from "../../config/constants";

const baseURL = `http://localhost:5000${URN.ENTITY}`;

describe('Seeding entities', () => {
    entitiesMocks.map((item) => {
        it(`Creating ${item.lorem}`, async (done) => {
            await axios
                .post(`${baseURL}`, item)
                .catch((e) => console.log(e));
            done();
        });
    });
})